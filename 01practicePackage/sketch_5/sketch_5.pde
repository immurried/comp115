
//GOALS draw a man as a contained object
//      be able to click and drag around man

Man m;

void setup()
{
  size(600, 400);
  background(0);
  
  m = new Man();
  
  m.posX = width /2;
  m.posY = width /2;
  
  m.dragBoxWidth = 100;
  m.dragBoxHeight = 100;
}



void draw()
{
  background(100);
  
  m.Draw();
  
  if (m.MouseInDragBox(mouseX, mouseY) && mousePressed)
  {
    m.posX = mouseX;
    m.posY = mouseY;
  }

}

class Man
{
  float posX;
  float posY;
  float dragBoxWidth;
  float dragBoxHeight;
  
  float headRad = 30;
  float armLength = 50;
  float armHeight = 10;
  float halfWidth = width /2;
  float waistHeight = 65;
  float footDist = 50;
  float footHeight = 90;
  
  void Draw(){
    
    stroke(255);
    strokeWeight(3);
    
    //Body
    line (posX, posY + headRad /2, posX, posY + waistHeight);

    //Arms
    line (posX - armLength /2, posY + headRad /2 + armHeight, posX + armLength /2, posY + headRad /2 + armHeight);

    //LeftLeg
    line (posX, posY + waistHeight, posX - footDist /2, posY + footHeight);

    //RightLeg
    line (posX, posY + waistHeight, posX + footDist /2, posY + footHeight);
    
    noStroke();
    //Head
    ellipse (posX, posY, headRad, headRad);
  }
  
  boolean MouseInDragBox(float x, float y)
  {
    if (x > posX - (dragBoxWidth /2) && x < posX + (dragBoxWidth /2))
    {
      if (y > posY - (dragBoxHeight /2) && y < posY + (dragBoxHeight /2))
      {
        return true;
      }
    }
    
    return false;
  }
}
