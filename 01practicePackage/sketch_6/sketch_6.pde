float prevMouseX;
float prevMouseY;

void setup()
{
  size(600, 400);
  background(255, 255, 0);
  
  prevMouseX = 0;
  prevMouseY = 0;
 
}

void draw()
{
  
}

void mouseClicked()
{
  strokeWeight(3);
  line(prevMouseX, prevMouseY, mouseX, mouseY);
  prevMouseX = mouseX;
  prevMouseY = mouseY;
}
