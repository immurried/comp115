float rad = 20;

//PVector startPos = new PVector(rad, height / 2.0); 
//PVector endPos = new PVector(width - rad, height / 2.0);

float speed = 40;

void setup()
{
  frameRate(60);
}

//Object circ = new circle(50, 50, rad);

void draw()
{
  float frameTime = ((frameCount / speed) % PI * 2);
  
  
  background(204);
  circle(sin(frameTime) * 50 + 50, height / 2, rad);

  println(frameTime);
}
