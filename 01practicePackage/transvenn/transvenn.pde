background(255);
size(300, 300);
float circRad = width /2;
float opacity = 125;

noStroke();
fill(255, 0, 0, opacity);
circle(width /3, height /3, circRad);

noStroke();
fill(0, 255, 0, opacity);
circle(width - (width /3), height /3, circRad);

noStroke();
fill(0, 0, 255, opacity);
circle(width /2, height - (height /3), circRad);
