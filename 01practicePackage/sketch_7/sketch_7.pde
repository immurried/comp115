
float circleRad = 30;
float bgCol = 255;
float startRad;
Circle[] circles;

PVector r = new PVector(255, 0, 0);
PVector g = new PVector(0, 255, 0);
PVector b = new PVector(0, 0, 255);
PVector y = new PVector(255, 255, 0);

PVector up = new PVector(0, 1);
PVector down = new PVector (0, -1);
PVector left = new PVector (-1, 0);
PVector right = new PVector (1, 0);

void setup()
{
  background(bgCol);
  
  size(600, 400);
  
  circles = new Circle[4];
  
  circles[0] = new Circle();
  circles[0].Setup(r, up);
  circles[1] = new Circle();
  circles[1].Setup(g, down);
  circles[2] = new Circle();
  circles[2].Setup(b, right);
  circles[3] = new Circle();
  circles[3].Setup(y, left);
}

void draw()
{
  clearScreen();
  int i = 0;
  while(i < 4)
  {
    circles[i].Update();
    i++;
  }
}

void mouseClicked()
{  
  int i = 0; //<>//
  while(i < 4)
  {
    circles[i].Reset(mouseX, mouseY);
    i++;
  }
}

void clearScreen()
{
    background(bgCol);
}

class Circle
{
  PVector pos;
  PVector col;
  float alpha;
  float radius = 30;
  
  PVector displacement;
  float growthRate = 1;
  float alphaDecay = 1;
  
  void Setup(PVector c, PVector d)
  {
    pos = new PVector();
    col = new PVector();
    displacement = new PVector();
    
    
    Reset(-9999, -9999);
    alpha = 0;
    
    col = c;
    displacement = d;
  }
  
  void Reset(float x, float y)
  {
    pos.x = x;
    pos.y = y;
    radius = startRad;
    alpha = 255;
  }
  
  void Update()
  {
    pos.add(displacement);
    alpha -= alphaDecay;
    radius += growthRate;
    Draw();
  }
  
  void Draw()
  {
    noStroke();
    fill(col.x, col.y, col.z, alpha);
    circle(pos.x, pos.y, radius);
  }
  
}
