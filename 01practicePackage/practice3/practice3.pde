size(600, 400);

background(30, 180, 30);

float boxBuffer = 20;

float circRad = 150;

float plusLength = width /3;
float plusThickness = height /10;


//white border
fill(0, 0, 0, 0);
stroke(255);
strokeWeight(10);
rect(boxBuffer, boxBuffer, width - boxBuffer * 2, height - boxBuffer * 2);

//circle
noStroke();
fill(255, 255, 0);
circle(width /2, height /2, circRad);

//horizontal arm
stroke(255, 155, 50);
strokeWeight(3);
fill(50, 50, 50, 150);
rect((width /2) - (plusLength /2), (height /2) - (plusThickness /2), plusLength, plusThickness);

//vert arm

stroke(255, 155, 50);
strokeWeight(3);
fill(50, 50, 50, 150);
rect((width /2) - (plusThickness /2), (height /2) - (plusLength /2), plusThickness, plusLength);
