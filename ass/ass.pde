// GOAL make two circles move across the srceen horizontally and vertically
//      such that they meet at the center of the screen independent of 
//      screen size. The movement should also be time-based such that it
//      is independent of framerate and meets the middle of the screen
//      at the same time

// variables for x and y coords to be used for movement
float x, y = 0;
float greenCircleDia;
float redCircleDia;

float vertLaneWidth;
float horizLaneHeight;

float speedMod = 1;

// timeToCenter is the time in sec to reach the middle of the screen
// timeToEnd is used to simplify the movement calculation
float timeToCenter = 2.5;
float timeToEnd = timeToCenter * 2;

float prevMillis = 0;
float deltaTime = 0;

void setup()
{
  size(500, 1200);
  
  vertLaneWidth = width /8;
  horizLaneHeight = height /8;
  
  greenCircleDia = horizLaneHeight * 0.8;
  redCircleDia = vertLaneWidth * 0.8;
  
  x = greenCircleDia /2;
  y = redCircleDia /2;
  
  frameRate(1000);
}

void draw()
{
  // Time elapsed since last frame
  deltaTime = millis() - prevMillis;
  // Convert deltaTime to seconds
  deltaTime = deltaTime / 1000.0;
  
  background(#C8F5F2); 
  
  DrawLanes();
  DrawCircles();
  
  x += (width - greenCircleDia) / timeToEnd * deltaTime * speedMod;
  y += (height - redCircleDia)  / timeToEnd * deltaTime * speedMod;
  
  StopCirclesAtWindowBounds();

  // Set prevMillis to current millis to be used next frame
  prevMillis = millis();
}

void DrawLanes()
{
  // Lane properties
  rectMode(CENTER);
  noStroke();
  fill(255);
  
  // Horizontal lane
  rect(width /2, height /2, width, horizLaneHeight);
  
  // Vert lane
  rect(width /2, height /2, vertLaneWidth, height);
}

void DrawCircles()
{
  //Horizontal circle
  fill(0, 255, 0, 150);
  circle(x, height /2, greenCircleDia);
  
  // Vertical circle
  fill(255, 0, 0, 150);
  circle(width /2, y, redCircleDia);
}

void StopCirclesAtWindowBounds()
{
  if ( x >= width - (greenCircleDia /2))
  {
    x = width - (greenCircleDia /2);
  }
  if (y >= height - (redCircleDia /2))
  {
    y = height - (redCircleDia /2);
  }
}

// Reset circles
void mousePressed()
{
  x = greenCircleDia /2;
  y = redCircleDia /2;
}

void keyPressed()
{
  // Up key doubles speed
  if (key == CODED && keyCode == UP)
  {
    speedMod = speedMod * 2;
  }
  // Down key halves speed
  if (key == CODED && keyCode == DOWN)
  {
    speedMod = speedMod /2;
  }
}
