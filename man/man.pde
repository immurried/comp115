float headRad = 30;
float armBuffer = 30;
float halfWidth = width /2;
float waistHeight = 65;

//Body
line (halfWidth, 30, halfWidth, waistHeight);

//Arms
line (armBuffer, 40, width - armBuffer, 40);

//LeftLeg
line (halfWidth, waistHeight, 40, 90);

//RightLeg
line (halfWidth, waistHeight, 60, 90);

//Head
ellipse (width /2, 16, headRad, headRad);
