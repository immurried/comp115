// circle collision by Zack Dingwall //<>// //<>// //<>//

// TODO mouse up - mouse down pos gives velocity
// TODO balls nudge size bigger on wall collision

ArrayList<Circle> circles;

void setup()
{
  frameRate(60);
  fullScreen();
  getSurface().setResizable(true);

  getSurface().setSize(600, 400);

  circles = new ArrayList<Circle>();
}

void draw()
{
  background(255);

  for (int i = 0; i < circles.size(); i++)
  {
    Circle c = circles.get(i);

    c.Update();

    for (int j = 0; j < circles.size(); j++)
    {
      Circle d = circles.get(j);

      if (c != d)
      {
        circleCollisionResolution(c, d);
      }
    }
  }
}

void mousePressed()
{
  SpawnCircle(mouseX, mouseY, pmouseX, pmouseY);
}

void SpawnCircle(float x, float y, float prevX, float prevY)
{
  Circle c = new Circle();

  circles.add(c);

  c.Setup(x, y, x - prevX, y - prevY);
}

void circleCollisionResolution(Circle a, Circle b)
{
  if (sqrt( sq(b.pos.x - a.pos.x) + sq(b.pos.y - a.pos.y) ) < b.radius + a.radius)
  {
    PVector BtoA = new PVector(a.pos.x - b.pos.x, a.pos.y - b.pos.y);

    PVector AtoB = new PVector(-BtoA.x, -BtoA.y);

    b.vel = AtoB;
    a.vel = BtoA;

    b.vel.normalize();
    a.vel.normalize();

    b.pos.x += b.vel.x;
    b.pos.y += b.vel.y;
    a.pos.x += a.vel.x;
    a.pos.y += a.vel.y;
  }
}

class Circle
{
  PVector pos = new PVector();
  PVector vel = new PVector();

  PVector col = new PVector(255, 0, 0);
  float diameter = 40;
  float radius = diameter /2;
  float speed = 5;

  void Setup(float x, float y, float vX, float vY)
  {
    pos = new PVector(x, y);
    vel = new PVector(vX, vY);
  }

  void Update()
  {
    vel.normalize();

    pos.x += vel.x * speed;
    pos.y += vel.y * speed;

    //left wall
    if (pos.x < 0 + radius)
    {
      vel.x = -vel.x;
      pos.x = radius + 1;
    }
    //right wall
    if (pos.x > width - radius)
    {
      getSurface().setSize(width + 1, height);
      vel.x = -vel.x;
      pos.x = width - radius - 1;
    }
    //top wall
    if (pos.y < 0 + radius)
    {
      vel.y = -vel.y;
      pos.y = radius + 1;
    }
    //bottom wall
    if (pos.y > height - radius)
    {
      getSurface().setSize(width, height + 1);
      vel.y = -vel.y;
      pos.y = height - radius - 1;
    }

    Draw();
  }

  void Draw()
  {
    fill(col.x, col.y, col.z);
    circle(pos.x, pos.y, diameter);
  }
}
